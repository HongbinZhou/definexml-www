.PHONY: start install serve build i18n-upload i18n-download

.DEFAULT_GOAL := start
start:
	@make install
	@make serve

install: test-builddeps
	@git submodule update --init --recursive
	@bundle config mirror.https://rubygems.org https://gems.ruby-china.org
	@bundle install
	@npm install

serve: test-builddeps
	@npm start& bundle exec jekyll serve --incremental& wait

build: test-builddeps
	@npm build
	@bundle exec jekyll build

serve-production: test-builddeps
	@NODE_ENV=production npm run build
	@JEKYLL_ENV=production bundle exec jekyll serve

build-production: install test-builddeps
	@NODE_ENV=production npm run build
	@JEKYLL_ENV=production bundle exec jekyll build

crowdin-upload: test-crowdin
	@crowdin-cli upload sources --auto-update -b master

crowdin-download: test-crowdin
	@crowdin-cli download -b master
	@ruby ./scripts/remove-unused-languages.rb
	@ruby ./scripts/normalize-translations.rb

###
# Misc stuff:
###

BUNDLE_EXISTS := $(shell command -v bundle 2> /dev/null)
CROWDIN_EXISTS := $(shell command -v crowdin-cli 2> /dev/null)
NPM_EXISTS := $(shell command -v npm 2> /dev/null)

test-builddeps:
ifndef BUNDLE_EXISTS
	$(error bundler is not installed. Run `gem install bundler`)
endif
ifndef NPM_EXISTS
	$(error npm is not installed. Follow the instructions on https://yarnpkg.com/docs/install)
endif

test-crowdin:
ifndef CROWDIN_EXISTS
	$(error Crowdin is not installed. Run `make install`)
endif
ifndef CROWDIN_API_KEY
	$(error CROWDIN_API_KEY is undefined)
endif

docker:
	docker run -it -v "$(PWD)":/srv/jekyll -p 127.0.0.1:4000:4000 jekyll/jekyll make start

docker-server:
	docker run -i --rm -v "$(PWD)":/srv/jekyll -p 127.0.0.1:4000:4000 jekyll/jekyll make build-production
